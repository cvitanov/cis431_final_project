#include <iostream>
#include <algorithm>
#include <math.h>
#include <random>
#include <cstdlib>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <unistd.h>
#include "helper.h"
#include "settings.h"

static int win_id; // window id
static unsigned int microseconds;
int count = 0;

// random number generator
static std::default_random_engine generator;


// BOID CLASS
class Boid {
  public:

    double size;
    double dt = DT;
    double ddt = DDT;
    vec p; // position
    vec v; // velocity vector
    vec newv; // new velocity
    vec avoidv; // avoidance vector
    double avoid_distance; // avoidance distance
    vec copyv; // copy vector
    vec centerv; // center vector
    vec viewv; // view vector
    vec boundv; // avoid boundaries
    int num;
    int window;
    double color[3];

    Boid(int window, int count);

    void plot(void);

    void refresh(void);

    void print(void);

    // update a boid's position and velocity
    void update(void);

    void displayBoid(void);

};

// FLOCK CLASS

class Flock {

    int size;
    double dt = DT;
    double ddt = DDT;
    int window;
    double mu = 0.5; // momentum factor
    double avoidw = AVOIDW;
    double vieww = VIEWW;
    double centerw = CENTERW;
    double copyw = COPYW;
    double boundw = BOUNDW; // avoiding boundaries
    double rview, rcent, rcopy, rvoid;
    double maxr;
    double angle, vangle, cosangle, cosvangle;
    vec vaverage; // average boid velocity
    vec paverage; // average boid position
    std::vector<Boid> boidList; // list of boids
    double rbound; // avoid bounding box
  
  public:

    // Flock constructor
    Flock(int flock_size, int window);

    // refresh flock display
    void refresh(void);

    // calculate new headings for boids in flock
    void calcHeadings(void);

    // update velocity and position vectors for boids
    void update(void);

    // print flock
    void print(void);

    // step
    void step(void) {
      calcHeadings();
      update();
      refresh();
    }

    void display();

};


void Boid::displayBoid(void) {

    glColor3f(color[0], color[1], color[2]);
    double angle;

    GLfloat col[] = {color[0], color[1], color[2], 1.f};

    glPushMatrix();

      glShadeModel(GL_SMOOTH);
      glLoadIdentity();
      glTranslatef( p.x, p.y, p.z);//p.z );

      angle = atan( newv.y / newv.z ) * 180.0 / PI;
      if(v.z < 0) {
        glRotatef( -angle,1.0,0.0,0.0);
      } else {
        glRotatef( -angle,1.0,0.0,0.0);
      }

      angle = atan( newv.y / newv.x ) * 180.0 / PI;
      if(v.x < 0) {
        glRotatef(angle + 90.0, 0.0, 0.0, 1.0);
      } else {
        glRotatef(angle - 90.0, 0.0, 0.0, 1.0);
      }

      glRotatef(-180.0, 0.0, 0.0, 0.0);

      glPushMatrix();
        glScalef(size,size,size);
        drawPlane();
      glPopMatrix();

    glPopMatrix();

    glLoadIdentity();

}

// init a new boid
Boid::Boid(int window, int count) {

  // calculate bounding box

  // setup random position and initial velocity for boid

  std::uniform_real_distribution<double> wdist(-1 * xbound, xbound);
  std::uniform_real_distribution<double> hdist(-1 * ybound, ybound);
  std::uniform_real_distribution<double> zdist(ZMIN, ZMAX);
  std::uniform_real_distribution<double> vdist(-1.0, 1.0);
  std::uniform_real_distribution<double> colorz(0.0, 0.7);

  // position
  p.x = wdist(generator);
  p.y = hdist(generator);
  p.z = zdist(generator);

  // velocity
  v.x = vdist(generator);
  v.y = vdist(generator);
  v.z = vdist(generator);

  // color
  color[0] = colorz(generator);
  color[1] = colorz(generator);
  color[2] = colorz(generator);

  size = BOIDSIZE;

  avoid_distance = 5;

  this->window = window;

  this->num = count;


}

// plot the boid shape
void Boid::plot(void) {
  // TODO
  return;
}

// refresh boid shape
void Boid::refresh(void) {
  // TODO
  return;
}

// print boid information
void Boid::print(void) {
  std::cout << "Boid #" << num << " pos = (";
  std::cout << p.x << ", " << p.y << ", " << p.z << ") ";
  std::cout << " - vel = (" << v.x << ", " << v.y << ", " << v.z << ")";
  std::cout << std::endl;
}

// update position and velocity
void Boid::update(void) {
  double max_vel = MAXV;
  v = newv;

  // cap velocity components with MAXV
  if( v.x > max_vel ) v.x = max_vel;
  if( v.x < (-1.0 * max_vel) ) v.x = -1.0 * max_vel;

  if( v.y > max_vel ) v.y = max_vel;
  if( v.y < (-1.0 * max_vel) ) v.y = -1.0 * max_vel;

  if( v.z > max_vel ) v.z = max_vel;
  if( v.z < (-1.0 * max_vel) ) v.z = -1.0 * max_vel;

  p.x += v.x * dt;
  p.y += v.y * dt;
  p.z += v.z * dt;

  // wrap coordinates
  if(p.x < (-1 * xbound) ) {
    p.x += 2 * xbound;
  } else if(p.x >= xbound) {
    p.x -= 2 * xbound;
  }
  if(p.y < (-1 * ybound) ) {
    p.y += 2 * ybound;
  } else if(p.y >= ybound) {
    p.y -= 2 * ybound;
  }
  double depth = ZMAX - ZMIN;
  if(p.z < ZMIN) {
    p.z += depth;
  } else if(p.z >= ZMAX) {
    p.z -= depth;
  }
}




// display
void Flock::display() {
  glClear(GL_COLOR_BUFFER_BIT);

  double xb = xbound + 1.0;
  double yb = ybound + 1.0;

  glColor3f(0,0,0);
  glBegin(GL_QUADS);
    glVertex3f(-xb, yb, ZMAX);
    glVertex3f(xb, yb, ZMAX);
    glVertex3f(xb, yb, ZMIN);
    glVertex3f(-xb, yb, ZMIN);
  glEnd();

  glBegin(GL_QUADS);
    glColor3f(0.0,0.0, 0.9);
    glVertex3f(-xb, -yb, ZMIN);
    glVertex3f(-xb, -yb, ZMAX);
    glColor3f(0.5,0.5,0.5);
    glVertex3f(-xb, yb, ZMAX);
    glVertex3f(-xb, yb, ZMIN);
  glEnd();
  glBegin(GL_QUADS);
    glColor3f(0.0,0.0, 0.9);
    glVertex3f(xb, -yb, ZMIN);
    glVertex3f(xb, -yb, ZMAX);
    glColor3f(0.5,0.5,0.5);
    glVertex3f(xb, yb, ZMAX);
    glVertex3f(xb, yb, ZMIN);
  glEnd();
  glBegin(GL_QUADS);
    glColor3f(0.0,0.0, 0.3);
    glVertex3f(-xb, -yb, ZMIN);
    glVertex3f(-xb, -yb, ZMAX);
    glVertex3f(xb, -yb, ZMAX);
    glVertex3f(xb, -yb, ZMIN);
  glEnd();
  glBegin(GL_QUADS);
    glColor3f(0.0,0.0, 0.9);
    glVertex3f(-xb, -yb, ZMAX);
    glVertex3f(xb, -yb, ZMAX);
    glColor3f(0.5,0.5,0.5);
    glVertex3f(xb, yb, ZMAX);
    glVertex3f(-xb, yb, ZMAX);
  glEnd();


  for (auto & b : boidList) {
    b.displayBoid();
  }
  glFlush();
}

// flock constructor
Flock::Flock(int flock_size, int window) {

  this->size = flock_size;
  // initialize list of boids for this flock
  for(int i = 0; i < size; ++i) {
    // add new boid to list of boids
    this->boidList.emplace_back(window, i);
    // TODO: plot this boid
  }

  // Radius values to use for flocking algorithm
  rview = RVIEW;
  rcopy = RCOPY;
  rcent = RCENT;
  rvoid = RVOID;

  // radius to avoid bounding box (not part of flocking algorithm)
  rbound = RBOUND; // radius to avoid boundaries

  // maximum radius
  maxr = std::max(rview, std::max(rcopy, std::max(rcent, rvoid)));

  // view angle values
  angle = ANGLE;
  vangle = VANGLE;
  angle = angle * PI / 180.0;
  vangle = vangle * PI / 180.0;
  cosangle = cos(angle / 2);
  cosvangle = cos(vangle / 2);
}

// refresh flock
void Flock::refresh(void) {
  for(auto & b : boidList) {
    b.refresh();
  }
}

// calcHeadings
void Flock::calcHeadings(void) {
  // min x,y,z
  double mx, my, mz;

  // loop through all boids in flock
  // NOTE: Opportunity for parallelism here!
  for(int i = 0; i < size; ++i) {

    // count of boids centering in on other boids
    int numcent = 0;

    // list of boids
    Boid & b = boidList[i];

    // zero out vectors for updating heading
    b.avoidv.zero();
    b.copyv.zero();
    b.centerv.zero();
    b.viewv.zero();
    b.boundv.zero();

    // calculate vector heading to avoid boundaries
    // inversely proportional to square of distance from boundary (q value is for this)
    if( b.p.x > (xbound - rbound) ) {
      double q = std::max(0.01, xbound - b.p.x);
      b.boundv.x = -1.0 / (q * q); // head away from boundary
    } else if ( b.p.x < (-1.0 * xbound + rbound) ) {
      double q = std::max(0.01, (-1.0 * xbound + rbound) - b.p.x);
      b.boundv.x = 1.0 / (q * q); // head away from boundary
    }
    if( b.p.y > (ybound - rbound) ) {
      double q = std::max(0.01, ybound - b.p.y);
      b.boundv.y = -1.0 / (q * q); // head away from boundary
    } else if ( b.p.y < (-1.0 * ybound + rbound) ) {
      double q = std::max(0.01, (-1.0 * ybound + rbound) - b.p.y);
      b.boundv.y = 1.0 / (q * q); // head away from boundary
    }
    if( b.p.z > (ZMAX - rbound) ) {
      double q = std::max(0.01, ZMAX - b.p.z);
      b.boundv.z = -1.0 / (q * q); // head away from boundary
    } else if ( b.p.z < (ZMIN + rbound) ) {
      double q = std::max(0.01, b.p.z - ZMIN);
      b.boundv.z = 1.0 / (q * q); // head away from boundary
    }

    // loop through other boids to update heading calculations
    // based on which other boids meet criteria
    // NOTE: Another Loop that could be parallelized?
    for(int j = 0; j < size; ++j) {

      // other boid to compare with
      Boid & other = boidList[j];

      // skip identical boid
      if(j == i) continue;

      double mindist = dist( b.p, other.p );
      mx = other.p.x;
      my = other.p.y;
      mz = other.p.z;

      // skip rest of calculations if this boid is further than rule radii
      if( mindist > maxr ) continue;

      // vector from first boid to other boid
      vec temp(0.0, 0.0, 0.0);
      temp.x = mx - b.p.x;
      temp.y = my - b.p.y;
      temp.z = mz - b.p.z;

      // is the other boid in the first boid's field of view?
      // use cosine of vector pointing between voids
      // along with first boid's velocity vector
      double costemp = dot(b.v, temp) / (len(b.v) * len(temp));
      if(costemp < cosangle) continue;

      // center in on boid if within center rule radius but outside avoid radius
      if(mindist <= RCENT and mindist > RVOID) {
        // calculate centering vector
        vec cvec( mx - b.p.x, my - b.p.y, mz - b.p.z);
        // centering vector magnitude is proportional to distance
        double d = len(cvec);
        b.centerv.x += (mx - b.p.x) * d;
        b.centerv.y += (my - b.p.y) * d;
        b.centerv.z += (mz - b.p.z) * d;
        numcent += 1;
      }

      // if within copy radius but outside avoid radius copy boid's velocity
      if(mindist <= RCOPY and mindist > RVOID) {
        b.copyv.x += other.v.x;
        b.copyv.y += other.v.y;
        b.copyv.z += other.v.z;
      }

      // within collision range, so avoid other boid
      if(mindist <= RVOID) {
        // calculate avoidance vector
        vec collide( b.p.x - mx, b.p.y - my, b.p.z - mz);
        // avoidance vector magnitude is proportional to distance
        double d = 1 / len(collide);
        collide.x = collide.x * d;
        collide.y = collide.y * d;
        collide.z = collide.z * d;
        b.avoidv.x += collide.x;
        b.avoidv.y += collide.y;
        b.avoidv.z += collide.z;
      }

      // try to move so other boid does not block view
      // NOTE: just using x and y coordinates for this
      vec block( other.p.x - b.p.x, other.p.y - b.p.y, other.p.z - b.p.z );
      double cosblock = dot(b.v, block) / (len(b.v) * len(block));
      if(mindist <= RVIEW and cosblock < cosvangle) {

        double u = 0;
        double v = 0;
        double w = 0;

        // add vector to move away from other boid
        u = b.v.x - block.x;
        v = b.v.y - block.y;
        w = b.v.z - block.z;

        // inversely proportional to distance
        double d = sqrt( block.x * block.x + block.y * block.y + block.z * block.z);
        if(d != 0) {
          u = u / d;
          v = v / d;
          w = w / d;
        }
        b.viewv.x += u;
        b.viewv.y += v;
        b.viewv.z += w;

      }



    } // end inner loop

    // avoid centering on just one boid
    if(numcent < 2) b.centerv.zero();

    // normalize vectors
    double l = len(b.centerv);
    if(l > 1.0) {
      b.centerv.x /= l;
      b.centerv.y /= l;
      b.centerv.z /= l;
    }
    l = len(b.avoidv);
    if(l > 1.0) {
      b.avoidv.x /= l;
      b.avoidv.y /= l;
      b.avoidv.z /= l;
    }
    l = len(b.viewv);
    if(l > 1.0) {
      b.viewv.x /= l;
      b.viewv.y /= l;
      b.viewv.z /= l;
    }
    l = len(b.copyv);
    if(l > 1.0) {
      b.copyv.x /= l;
      b.copyv.y /= l;
      b.copyv.z /= l;
    }
    l = len(b.boundv);
    if(l > 1.0) {
      b.boundv.x /= l;
      b.boundv.y /= l;
      b.boundv.z /= l;
    }

    // compute weighted trajectory
    vec t(0.0,0.0,0.0); // trajectory
    t.x += b.centerv.x * centerw;
    t.x += b.copyv.x * copyw;
    t.x += b.avoidv.x * avoidw;
    t.x += b.viewv.x * vieww;
    t.x += b.boundv.x * boundw;

    t.y += b.centerv.y * centerw;
    t.y += b.copyv.y * copyw;
    t.y += b.avoidv.y * avoidw;
    t.y += b.viewv.y * vieww;
    t.y += b.boundv.y * boundw;

    t.z += b.centerv.z * centerw;
    t.z += b.copyv.z * copyw;
    t.z += b.avoidv.z * avoidw;
    t.z += b.viewv.z * vieww;
    t.z += b.boundv.z * boundw;

    // add noise if desired
    if(WRAND > 0) {
      std::uniform_real_distribution<double> randnoise(-1,1);
      t.x += randnoise(generator) * WRAND;
      t.y += randnoise(generator) * WRAND;
      t.z += randnoise(generator) * WRAND;
    }

    // update velocity and renormalize
    b.newv.x = b.v.x * ddt + t.x * (1 - ddt);
    b.newv.y = b.v.y * ddt + t.y * (1 - ddt);
    b.newv.z = b.v.z * ddt + t.z * (1 - ddt);
    double d = len(b.newv);
    if(d < MINV) {
      b.newv.x *= MINV / d;
      b.newv.y *= MINV / d;
      b.newv.z *= MINV / d;
    }

  } // end outer loop

} // end calcHeadings

// update flock
void Flock::update(void) {
  for(auto & b : boidList) {
    b.update();
  }
}

// print flock
void Flock::print(void) {
  for(auto & b : boidList) {
    b.print();
  }
}

static Flock theFlock(NBOIDS, WINDOW);

void display(void) {
  glMatrixMode(GL_MODELVIEW);
  theFlock.display();
  glFlush();
}

void idle(void) {
  theFlock.step();
}

void drive (int data) {

  glutTimerFunc(FRATE, drive, -1); // call drive() again in 30 milliseconds
  theFlock.step();
  if(DEBUG) {
    theFlock.print(); // print flock data
  }

  glClearDepth(1.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glutPostRedisplay();

}


void keyboard( unsigned char key, int x, int y )
{
  switch ( key )
  {
    case 27: // Escape key
      glutDestroyWindow ( win_id );
      exit (0);
      break;
  }
  glutPostRedisplay();
}

// MAIN

int main(int argc, char **argv) {

  if(DEBUG) std::cout << " x/y bounds are " << xbound << ", " << ybound << std::endl;

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(W, H);
  win_id = glutCreateWindow("Fishies");

  glutReshapeFunc(reshape);
  glutTimerFunc(FRATE, drive, -1);
  glutDisplayFunc(display);
  glutKeyboardFunc(keyboard);
  glutMainLoop();

} // end main
