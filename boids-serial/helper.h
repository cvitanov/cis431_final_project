// HELPER FUNCTIONS
#ifndef HELPER_
  #define HELPER_
#endif

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <random>
#include "settings.h"

//bounding box (to keep boids swimming within sight of the virtual camera)
static double ybound = ZMIN * tan( (FOVY / 2) * PI / 180.0 );
static double xbound = W * ybound / H;


// 3D Vector Struct
class vec {
  public:

    double x,y,z;

    // constructors
    vec (void) {
      x = 0;
      y = 0;
      z = 0;
    }

    vec (double x_, double y_, double z_) : x(x_), y(y_), z(z_) {};

    // overload assignment
    void operator = ( const vec &V ) {
      x = V.x;
      y = V.y;
      z = V.z;
    };

    // zero vector
    void zero(void) {
      x = 0.0;
      y = 0.0;
      z = 0.0;
    }

};

// Return a random float in the range 0.0 to 1.0.
GLfloat randomFloat();

// reshape display for graphics
void reshape(int, int);

// len: returns length of 3D vector
double len(vec);

// dist: returns distance between two vectors
double dist(vec, vec);

// dot: returns dot product of two vectors
double dot(vec, vec);

// draw plane (3D OpenGL Object)
void drawPlane();
