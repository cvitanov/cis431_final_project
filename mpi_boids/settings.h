#ifndef SETTINGS_
  #define SETTINGS_
#endif

// BOID DIMENSIONS AND BOID COUNT
#define BOIDSIZE 0.02
#define BOIDWIDTH 3
//#define NBOIDS 30 // not used
#define MAXV 0.1

// WEIGHTS USED FOR UPDATES
#define CENTERW 1.50
#define AVOIDW 3.00
#define VIEWW 2.0//2.00
#define COPYW 1.00

// TIME STEP
#define DT .1
#define DDT 0.95

// RADIUS USED FOR UPDATES
#define RCOPY 1.7
#define RCENT 1.5
#define RVIEW 1.2
#define RVOID 0.15

// OTHER CONSTANTS
#define MINV 0.5
#define WRAND 0.1
#define WINDOW 0
#define FRATE 20 // frame duration (ms)

// BOID VIEWING ANGLES
#define ANGLE 270.0
#define VANGLE 120.0

// WINDOW DIMENSIONS
#define W 800
#define H 600

// VIEWING SETTINGS
#define FOVY 45.0 // field of view angle
#define ZMIN 5.0
#define ZMAX 25.0
#define RBOUND 0.5
#define BOUNDW 10.0

// VIEW FULLSCREEN?
#define FULLSCREEN 0

// DEBUG
#define DEBUG 0

// use orthographic projection
#define ORTHO 0

// Timer Count to use for averaging calc headings timing result
#define TCOUNT 10

#define PI 3.14159265

extern double camera_location;
