#include "mpi.h"
#include <cstddef> // calculating structure data member offsets
#include <iostream>
#include <chrono>
#include <algorithm>
#include <math.h>
#include <random>
#include <cstdlib>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <unistd.h>
#include "helper.h"
#include "settings.h"
#define MASTER 0
#define MAX_THREADS 8

static int win_id; // window id
static unsigned int microseconds;
int count = 0;
int nboids;
extern double nthreads = 1;
extern int sendCount[MAX_THREADS]; // count of boids to send to slave threads

// MPI DATATYPE FOR SHARING
MPI_Datatype mpi_boid_datatype;


//BOID DATA STRUCTURE FOR MPI
typedef struct bData_s {

  double size;
  double dt = DT;
  double ddt = DDT;
  double p[3];
  double v[3];
  double newv[3];
  double avoidv[3];
  double avoid_distance;
  double copyv[3];
  double centerv[3];
  double viewv[3];
  double boundv[3];
  int num;
  int window;
  double color[3];


} bData;

// random number generator
static std::default_random_engine generator;

// master and slave processes
void master(int, char**);
void slave(int);

// BOID CLASS
class Boid {
  public:

    bData data;

    Boid(int window, int count);

    Boid(bData data);

    // Copy constructor
    Boid(const Boid &b2) {data = b2.data;}

    void plot(void);

    void refresh(void);

    void print(void);

    // update a boid's position and velocity
    void update(void);

    void displayBoid(void);

};

// FLOCK CLASS

class Flock {

    typedef std::chrono::high_resolution_clock Clock;
    double timer_sum;
    int timer_count;

    int size;
    double dt = DT;
    double ddt = DDT;
    int window;
    double mu = 0.5; // momentum factor
    double avoidw = AVOIDW;
    double vieww = VIEWW;
    double centerw = CENTERW;
    double copyw = COPYW;
    double boundw = BOUNDW; // avoiding boundaries
    double rview, rcent, rcopy, rvoid;
    double maxr;
    double angle, vangle, cosangle, cosvangle;
    vec vaverage; // average boid velocity
    vec paverage; // average boid position
    std::vector<Boid> boidList; // list of boids
    double rbound; // avoid bounding box

  public:

    // Flock constructor
    Flock(int flock_size, int window);

    // refresh flock display
    void refresh(void);

    // calculate new headings for boids in flock
    void calcHeadings(void);

    // update velocity and position vectors for boids
    void update(void);

    // print flock
    void print(void);

    // step
    void step(void) {
      auto t1 = Clock::now();

      calcHeadings();

      auto t2 = Clock::now();

      if(timer_count < TCOUNT)
        timer_sum += std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();

      if(timer_count++ == TCOUNT) {
        double average_calc_time = timer_sum / timer_count;
        std::cout << "Average time to calulate headings = " << average_calc_time << " ms" << std::endl;
      }
      update();
      refresh();
    }

    void display();

    void resetSize(int);

};


void Boid::displayBoid(void) {

    glColor3f(data.color[0], data.color[1], data.color[2]);
    double angle;

    GLfloat col[] = {data.color[0], data.color[1], data.color[2], 1.f};

    glPushMatrix();

      glShadeModel(GL_SMOOTH);
      glLoadIdentity();
      glTranslatef( data.p[0], data.p[1], data.p[2]);//data.p[2] );

      angle = atan( data.newv[1] / data.newv[2] ) * 180.0 / PI;
      if(data.v[2] < 0) {
        glRotatef( -angle,1.0,0.0,0.0);
      } else {
        glRotatef( -angle,1.0,0.0,0.0);
      }

      angle = atan( data.newv[1] / data.newv[0] ) * 180.0 / PI;
      if(data.v[0] < 0) {
        glRotatef(angle + 90.0, 0.0, 0.0, 1.0);
      } else {
        glRotatef(angle - 90.0, 0.0, 0.0, 1.0);
      }

      glRotatef(-180.0, 0.0, 0.0, 0.0);

      glPushMatrix();
        glScalef(data.size,data.size,data.size);
        drawPlane();
      glPopMatrix();

    glPopMatrix();

    glLoadIdentity();

}

// new boid from data
Boid::Boid(bData data) {
  this->data = data;
}

// init a new boid
Boid::Boid(int window, int count) {

  // calculate bounding box

  // setup random position and initial velocity for boid

  std::uniform_real_distribution<double> wdist(-1 * xbound, xbound);
  std::uniform_real_distribution<double> hdist(-1 * ybound, ybound);
  std::uniform_real_distribution<double> zdist(ZMIN, ZMAX);
  std::uniform_real_distribution<double> vdist(-1.0, 1.0);
  std::uniform_real_distribution<double> colorz(0.0, 0.7);

  // position
  data.p[0] = wdist(generator);
  data.p[1] = hdist(generator);
  data.p[2] = zdist(generator);

  // velocity
  data.v[0] = vdist(generator);
  data.v[1] = vdist(generator);
  data.v[2] = vdist(generator);

  // color
  data.color[0] = colorz(generator);
  data.color[1] = colorz(generator);
  data.color[2] = colorz(generator);

  data.size = BOIDSIZE;

  data.avoid_distance = 5;

  data.window = window;

  data.num = count;


}

// plot the boid shape
void Boid::plot(void) {
  // TODO
  return;
}

// refresh boid shape
void Boid::refresh(void) {
  // TODO
  return;
}

// print boid information
void Boid::print(void) {
  std::cout << "Boid #" << data.num << " pos = (";
  std::cout << data.p[0] << ", " << data.p[1] << ", " << data.p[2] << ") ";
  std::cout << " - vel = (" << data.v[0] << ", " << data.v[1] << ", " << data.v[2] << ")";
  std::cout << " - color = " << data.color[0] << " " << data.color[1] << " " << data.color[2] << std::endl;
  std::cout << std::endl;
}

// update position and velocity
void Boid::update(void) {

  double max_vel = 2.0;


  for(int k = 0; k < 3; ++k)
    data.v[k] = data.newv[k];


  // cap velocity components with MAXV
  if( data.v[0] > max_vel ) data.v[0] = max_vel;
  if( data.v[0] < (-1.0 * max_vel) ) data.v[0] = -1.0 * max_vel;

  if( data.v[1] > max_vel ) data.v[1] = max_vel;
  if( data.v[1] < (-1.0 * max_vel) ) data.v[1] = -1.0 * max_vel;

  if( data.v[2] > max_vel ) data.v[2] = max_vel;
  if( data.v[2] < (-1.0 * max_vel) ) data.v[2] = -1.0 * max_vel;

  data.p[0] += data.v[0] * data.dt;
  data.p[1] += data.v[1] * data.dt;
  data.p[2] += data.v[2] * data.dt;


  // wrap coordinates
  if(data.p[0] < (-1 * xbound) ) {
    data.p[0] += 2 * xbound;
  } else if(data.p[0] >= xbound) {
    data.p[0] -= 2 * xbound;
  }
  if(data.p[1] < (-1 * ybound) ) {
    data.p[1] += 2 * ybound;
  } else if(data.p[1] >= ybound) {
    data.p[1] -= 2 * ybound;
  }
  double depth = ZMAX - ZMIN;
  if(data.p[2] < ZMIN) {
    data.p[2] += depth;
  } else if(data.p[2] >= ZMAX) {
    data.p[2] -= depth;
  }
}




// display
void Flock::display() {
  glClear(GL_COLOR_BUFFER_BIT);

  double xb = xbound + 1.0;
  double yb = ybound + 1.0;

  glColor3f(0,0,0);
  glBegin(GL_QUADS);
    glVertex3f(-xb, yb, ZMAX);
    glVertex3f(xb, yb, ZMAX);
    glVertex3f(xb, yb, ZMIN);
    glVertex3f(-xb, yb, ZMIN);
  glEnd();

  glBegin(GL_QUADS);
    glColor3f(0.0,0.0, 0.9);
    glVertex3f(-xb, -yb, ZMIN);
    glVertex3f(-xb, -yb, ZMAX);
    glColor3f(0.5,0.5,0.5);
    glVertex3f(-xb, yb, ZMAX);
    glVertex3f(-xb, yb, ZMIN);
  glEnd();
  glBegin(GL_QUADS);
    glColor3f(0.0,0.0, 0.9);
    glVertex3f(xb, -yb, ZMIN);
    glVertex3f(xb, -yb, ZMAX);
    glColor3f(0.5,0.5,0.5);
    glVertex3f(xb, yb, ZMAX);
    glVertex3f(xb, yb, ZMIN);
  glEnd();
  glBegin(GL_QUADS);
    glColor3f(0.0,0.0, 0.3);
    glVertex3f(-xb, -yb, ZMIN);
    glVertex3f(-xb, -yb, ZMAX);
    glVertex3f(xb, -yb, ZMAX);
    glVertex3f(xb, -yb, ZMIN);
  glEnd();
  glBegin(GL_QUADS);
    glColor3f(0.0,0.0, 0.9);
    glVertex3f(-xb, -yb, ZMAX);
    glVertex3f(xb, -yb, ZMAX);
    glColor3f(0.5,0.5,0.5);
    glVertex3f(xb, yb, ZMAX);
    glVertex3f(-xb, yb, ZMAX);
  glEnd();


  for (auto & b : boidList) {
    b.displayBoid();
  }
  glFlush();
}

// flock constructor
Flock::Flock(int flock_size, int window) {

  this->timer_sum = 0;
  this->timer_count = 0;

  this->size = flock_size;
  // initialize list of boids for this flock
  for(int i = 0; i < size; ++i) {
    // add new boid to list of boids
    this->boidList.emplace_back(window, i);
    // TODO: plot this boid
  }

  // Radius values to use for flocking algorithm
  rview = RVIEW;
  rcopy = RCOPY;
  rcent = RCENT;
  rvoid = RVOID;

  // radius to avoid bounding box (not part of flocking algorithm)
  rbound = RBOUND; // radius to avoid boundaries

  // maximum radius
  maxr = std::max(rview, std::max(rcopy, std::max(rcent, rvoid)));

  // view angle values
  angle = ANGLE;
  vangle = VANGLE;
  angle = angle * PI / 180.0;
  vangle = vangle * PI / 180.0;
  cosangle = cos(angle / 2);
  cosvangle = cos(vangle / 2);
}

void Flock::resetSize(int nboids_) {

  this->size = nboids_;

  // initialize list of boids for this flock
  for(int i = 0; i < size; ++i) {
    // add new boid to list of boids
    this->boidList.emplace_back(window, i);
    // TODO: plot this boid
  }
}

// refresh flock
void Flock::refresh(void) {
  for(auto & b : boidList) {
    b.refresh();
  }
}

// calcHeadings
void Flock::calcHeadings(void) {

  double start_time, end_time;
  int nprocs, nslaves, min_boid_count, extra_boids, bcount;


  bData blist[nboids];
  bData * blist_ptr = &blist[0];
  bData recv_buffer[nboids];
  bData * chunk_ptr = &recv_buffer[0];

  MPI_Status status;

  // send work to slave threads and then wait to recieve results back
  start_time = MPI_Wtime();

  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  nslaves = nprocs - 1;
  min_boid_count = floor(nboids / nslaves);
  extra_boids = nboids - (nslaves * min_boid_count);

  // broadcast data to boids
  for(int i = 0; i < nboids; ++i) {
    Boid temp = boidList[i];
    blist[i] = temp.data;
  }

  MPI_Bcast(blist_ptr, nboids, mpi_boid_datatype, MASTER, MPI_COMM_WORLD);

  // receive data back from boids
  for(int s = 1; s <= nslaves; ++s) {

    int slave_pid = s;
    bcount = (slave_pid == nslaves) ? (min_boid_count + extra_boids) : min_boid_count;


    // determine which boids are being sent back
    int count = slave_pid;
    chunk_ptr = &recv_buffer[0];

    // determine start point for ptr to this slave's assigned chunk
    while(count > 1) {
      chunk_ptr += min_boid_count;
      count--;
    }

    // wait to receive data;
    MPI_Recv(chunk_ptr, bcount, mpi_boid_datatype, slave_pid, 0, MPI_COMM_WORLD, &status);





  }

  //
  for(int i = 0; i < nboids; ++i) {
  }
  // done receiving updated boid data
  // now refresh boidList
  for(auto & b : boidList) {
    int id = b.data.num;

    b.data = recv_buffer[id];
  }

  end_time = MPI_Wtime();
  //std::cout << "Elapsed time = " << end_time - start_time << std::endl;


} // end calcHeadings

// update flock
void Flock::update(void) {
  for(auto & b : boidList) {
    b.update();
  }
}

// print flock
void Flock::print(void) {
  for(auto & b : boidList) {
    b.print();
  }
}

static Flock theFlock(nboids, WINDOW);

void display(void) {
  glMatrixMode(GL_MODELVIEW);
  //theFlock.print();
  theFlock.display();
  glFlush();
}

void idle(void) {
  theFlock.step();
}

void drive (int data) {

  glutTimerFunc(FRATE, drive, -1); // call drive() again in 30 milliseconds
  theFlock.step();
  if(DEBUG) {
    theFlock.print(); // print flock data
  }

  glClearDepth(1.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glutPostRedisplay();

}


void keyboard( unsigned char key, int x, int y )
{
  switch ( key )
  {

    case 27: // Escape key
      glutDestroyWindow ( win_id );
      exit (0);
      break;
  }
  glutPostRedisplay();
}

// MAIN

int main(int argc, char **argv) {

  if(DEBUG) std::cout << " x/y bounds are " << xbound << ", " << ybound << std::endl;

  if(argc < 2) {
    std::cout << "USAGE: mpirun -np NTHREADS ./boids_mpi NBOIDS (e.g. ./boids_serial 10 for simulating 10 boids)" << std::endl;
    exit(0);
  } else {
      nboids = atoi(argv[1]);

       // initialize processes
       MPI_Init(NULL, NULL);
       int pid;
       MPI_Comm_rank(MPI_COMM_WORLD, &pid);
       std::cout << "MPI process " << pid << " processing boids ...\n";

       // Create MPI Type for Boid Data (Send/Recv)
       const int nitems = 15;
       int blocklengths[nitems] = {1,1,1,3,3,3,3,1,3,3,3,3,1,1,3};
       MPI_Datatype types[nitems] = {
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE,
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE,
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE,
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE,
         MPI_INT, MPI_INT, MPI_DOUBLE
       };
       MPI_Aint offsets[nitems];

       offsets[0] = offsetof(bData, size);
       offsets[1] = offsetof(bData, dt);
       offsets[2] = offsetof(bData, ddt);
       offsets[3] = offsetof(bData, p);
       offsets[4] = offsetof(bData, v);
       offsets[5] = offsetof(bData, newv);
       offsets[6] = offsetof(bData, avoidv);
       offsets[7] = offsetof(bData, avoid_distance);
       offsets[8] = offsetof(bData, copyv);
       offsets[9] = offsetof(bData, centerv);
       offsets[10] = offsetof(bData, viewv);
       offsets[11] = offsetof(bData, boundv);
       offsets[12] = offsetof(bData, num);
       offsets[13] = offsetof(bData, window);
       offsets[14] = offsetof(bData, color);

       MPI_Type_create_struct(nitems, blocklengths, offsets, types, &mpi_boid_datatype);
       MPI_Type_commit(&mpi_boid_datatype);
       // {
       //     int i = 0;
       //     char hostname[256];
       //     gethostname(hostname, sizeof(hostname));
       //     printf("PID %d on %s ready for attach\n", getpid(), hostname);
       //     fflush(stdout);
       //     while (0 == i)
       //         sleep(5);
       // }

       if(pid == MASTER) {
         //std::cout << "NBOIDS  " << nboids << std::endl;
         theFlock.resetSize(nboids);
         master(argc,argv);
       } else {

         slave(pid);
       }

       MPI_Type_free(&mpi_boid_datatype);
       MPI_Finalize();

    }


  return 0;

} // end main

void master(int argc, char **argv) {

  //std::cout << "Master running\n";

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(W, H);
  win_id = glutCreateWindow("Fishies");
  glutReshapeFunc(reshape);
  glutTimerFunc(FRATE, drive, -1);
  glutDisplayFunc(display);
  glutKeyboardFunc(keyboard);
  glutMainLoop();
  //std::cout << "Running Main Loop\n";
}

void slave(int slave_pid) {

  double avoidw = AVOIDW;
  double vieww = VIEWW;
  double centerw = CENTERW;
  double copyw = COPYW;
  double boundw = BOUNDW; // avoiding boundaries

  // Radius values to use for flocking algorithm
  double rview = RVIEW;
  double rcopy = RCOPY;
  double rcent = RCENT;
  double rvoid = RVOID;

  // radius to avoid bounding box (not part of flocking algorithm)
  double rbound = RBOUND; // radius to avoid boundaries

  // maximum radius
  double maxr = std::max(rview, std::max(rcopy, std::max(rcent, rvoid)));

  double angle = ANGLE;
  double vangle = VANGLE;
  angle = angle * PI / 180.0;
  vangle = vangle * PI / 180.0;
  double cosangle = cos(angle / 2);
  double cosvangle = cos(vangle / 2);

  double ddt = DDT;

  bData blist[nboids];
  bData * blist_ptr = &blist[0];
  bData * chunk_ptr; // the slave's chunk to send back to master
  MPI_Status status;
  int src = MASTER;
  int nprocs, nslaves, min_boid_count, extra_boids, bcount;

  //std::cout << "Slave " << slave_pid << " running ... " << std::endl;

  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  nslaves = nprocs - 1;
  min_boid_count = floor(nboids / nslaves);
  extra_boids = nboids - (nslaves * min_boid_count);
  bcount = (slave_pid == nslaves) ? (min_boid_count + extra_boids) : min_boid_count;



  // listen continually for new boid data to process
  while(1) {

    //std::cout << "Slave " << slave_pid << " waiting ..." << std::endl;
    blist_ptr = &blist[0];
    MPI_Bcast(blist_ptr, nboids, mpi_boid_datatype, MASTER, MPI_COMM_WORLD);

    //std::cout << "Slave " << slave_pid << " Recv complete" << std::endl;

    // calculate new headings

    // min x,y,z
    double mx, my, mz;

    double rbound = RBOUND;

    int start;
    start = min_boid_count * (slave_pid - 1);
    // loop through all boids in flock
    // NOTE: Opportunity for parallelism here!
    for(int i = start; i < (start + bcount); ++i) {

      // count of boids centering in on other boids
      int numcent = 0;

      // list of boids
      bData & b = blist[i];
      //std::cout << "SLAVE  b.num " << b.num << std::endl;

      // zero out vectors for updating heading
      for(int k = 0; k < 3; ++k) {
        b.avoidv[k] = 0;
        b.copyv[k] = 0;
        b.centerv[k] = 0;
        b.viewv[k] = 0;
        b.boundv[k] = 0;
      }

      // calculate vector heading to avoid boundaries
      // inversely proportional to square of distance from boundary (q value is for this)
      if( b.p[0] > (xbound - rbound) ) {
        double q = std::max(0.01, xbound - b.p[0]);
        b.boundv[0] = -1.0 / (q * q); // head away from boundary
      } else if ( b.p[0] < (-1.0 * xbound + rbound) ) {
        double q = std::max(0.01, (-1.0 * xbound + rbound) - b.p[0]);
        b.boundv[0] = 1.0 / (q * q); // head away from boundary
      }
      if( b.p[1] > (ybound - rbound) ) {
        double q = std::max(0.01, ybound - b.p[1]);
        b.boundv[1] = -1.0 / (q * q); // head away from boundary
      } else if ( b.p[1] < (-1.0 * ybound + rbound) ) {
        double q = std::max(0.01, (-1.0 * ybound + rbound) - b.p[1]);
        b.boundv[1] = 1.0 / (q * q); // head away from boundary
      }
      if( b.p[2] > (ZMAX - rbound) ) {
        double q = std::max(0.01, ZMAX - b.p[2]);
        b.boundv[2] = -1.0 / (q * q); // head away from boundary
      } else if ( b.p[2] < (ZMIN + rbound) ) {
        double q = std::max(0.01, b.p[2] - ZMIN);
        b.boundv[0] = 1.0 / (q * q); // head away from boundary
      }

      // loop through other boids to update heading calculations
      // based on which other boids meet criteria
      // NOTE: Another Loop that could be parallelized?
      for(int j = 0; j < nboids; ++j) {

        // other boid to compare with
        bData & other = blist[j];

        // skip identical boid
        if(other.num == b.num) continue;

        vec bp(b.p[0], b.p[1], b.p[2]);
        vec otherp(other.p[0], other.p[1], other.p[2]);
        double mindist = dist( bp, otherp );
        mx = other.p[0];
        my = other.p[1];
        mz = other.p[2];

        // skip rest of calculations if this boid is further than rule radii
        if( mindist > maxr ) continue;

        // vector from first boid to other boid
        vec temp(0.0, 0.0, 0.0);
        temp.x = mx - b.p[0];
        temp.y = my - b.p[1];
        temp.z = mz - b.p[2];

        // is the other boid in the first boid's field of view?
        // use cosine of vector pointing between voids
        // along with first boid's velocity vector
        vec bv(b.v[0], b.v[1], b.v[2]);
        double costemp = dot(bv, temp) / (len(bv) * len(temp));
        if(costemp < cosangle) continue;

        // center in on boid if within center rule radius but outside avoid radius
        if(mindist <= RCENT and mindist > RVOID) {
          // calculate centering vector
          vec cvec( mx - b.p[0], my - b.p[1], mz - b.p[2]);
          // centering vector magnitude is proportional to distance
          double d = len(cvec);
          b.centerv[0] += (mx - b.p[0]) * d;
          b.centerv[1] += (my - b.p[1]) * d;
          b.centerv[2] += (mz - b.p[2]) * d;
          numcent += 1;
        }

        // if within copy radius but outside avoid radius copy boid's velocity
        if(mindist <= RCOPY and mindist > RVOID) {
          b.avoidv[0] += other.v[0];
          b.avoidv[1] += other.v[1];
          b.avoidv[2] += other.v[2];
        }

        // within collision range, so avoid other boid
        if(mindist <= RVOID) {
          // calculate avoidance vector
          vec collide( b.p[0] - mx, b.p[1] - my, b.p[2] - mz);
          // avoidance vector magnitude is proportional to distance
          double d = 1 / len(collide);
          collide.x = collide.x * d;
          collide.y = collide.y * d;
          collide.z = collide.z * d;
          b.avoidv[0] += collide.x;
          b.avoidv[1] += collide.y;
          b.avoidv[2] += collide.z;
        }

        // try to move so other boid does not block view
        // NOTE: just using x and y coordinates for this
        vec block( other.p[0] - b.p[0], other.p[1] - b.p[1], other.p[2] - b.p[2] );
        vec bvtemp(b.v[0], b.v[1], b.v[2]);
        double cosblock = dot(bvtemp, block) / (len(bvtemp) * len(block));
        if(mindist <= RVIEW and cosblock < cosvangle) {

          double u = 0;
          double v = 0;
          double w = 0;

          // add vector to move away from other boid
          u = b.v[0] - block.x;
          v = b.v[1] - block.y;
          w = b.v[2] - block.z;

          // inversely proportional to distance
          double d = sqrt( block.x * block.x + block.y * block.y + block.z * block.z);
          if(d != 0) {
            u = u / d;
            v = v / d;
            w = w / d;
          }
          b.viewv[0] += u;
          b.viewv[1] += v;
          b.viewv[2] += w;

        }



      } // end inner loop

      // avoid centering on just one boid
      if(numcent < 2) {
        for(int k = 0; k < 3; ++k) {
          b.centerv[k] = 0;
        }
      }

      // normalize vectors
      vec bcenterv(b.centerv[0], b.centerv[1], b.centerv[2]);
      double l = len(bcenterv);
      if(l > 1.0) {
        b.centerv[0] /= l;
        b.centerv[1] /= l;
        b.centerv[2] /= l;
      }
      vec bavoidv(b.avoidv[0], b.avoidv[1], b.avoidv[2]);
      l = len(bavoidv);
      if(l > 1.0) {
        b.avoidv[0] /= l;
        b.avoidv[1] /= l;
        b.avoidv[2] /= l;
      }
      vec bviewv(b.viewv[0], b.viewv[1], b.viewv[2]);
      l = len(bviewv);
      if(l > 1.0) {
        b.viewv[0] /= l;
        b.viewv[1] /= l;
        b.viewv[2] /= l;
      }
      vec bcopyv(b.copyv[0], b.copyv[1], b.copyv[2]);
      l = len(bcopyv);
      if(l > 1.0) {
        b.copyv[0] /= l;
        b.copyv[1] /= l;
        b.copyv[2] /= l;
      }
      vec bboundv(b.boundv[0], b.boundv[1], b.boundv[2]);
      l = len(bboundv);
      if(l > 1.0) {
        b.boundv[0] /= l;
        b.boundv[1] /= l;
        b.boundv[2] /= l;
      }

      // compute weighted trajectory


      vec t(0.0,0.0,0.0); // trajectory


      t.x += b.centerv[0] * centerw;
      t.x += b.copyv[0] * copyw;
      t.x += b.avoidv[0] * avoidw;
      t.x += b.viewv[0] * vieww;
      t.x += b.boundv[0] * boundw;

      t.y += b.centerv[1] * centerw;
      t.y += b.copyv[1] * copyw;
      t.y += b.avoidv[1] * avoidw;
      t.y += b.viewv[1] * vieww;
      t.y += b.boundv[1] * boundw;

      t.z += b.centerv[2] * centerw;
      t.z += b.copyv[2] * copyw;
      t.z += b.avoidv[2] * avoidw;
      t.z += b.viewv[2] * vieww;
      t.z += b.boundv[2] * boundw;




      // add noise if desired
      if(WRAND > 0) {
        std::uniform_real_distribution<double> randnoise(-1,1);
        t.x += randnoise(generator) * WRAND;
        t.y += randnoise(generator) * WRAND;
        t.z += randnoise(generator) * WRAND;
      }

      // update velocity and renormalize

      b.newv[0] = b.v[0] * ddt + t.x * (1 - ddt);
      b.newv[1] = b.v[1] * ddt + t.y * (1 - ddt);
      b.newv[2] = b.v[2] * ddt + t.z * (1 - ddt);
      vec bnewv(b.newv[0], b.newv[1], b.newv[2]);
      double d = len(bnewv);
      if(d < MINV) {
        b.newv[0] *= MINV / d;
        b.newv[1] *= MINV / d;
        b.newv[2] *= MINV / d;
      }


    } // end outer loop

    // send headings back to master

    // determine which boids to send back
    int count = slave_pid;
    chunk_ptr = blist_ptr;

    // determine start point for ptr to this slave's assigned chunk
    while(count > 1) {
      chunk_ptr += min_boid_count;
      count--;
    }

    for(int i = 0; i < bcount; ++i) {
      //std::cout << "CHUNK PTR at i = " << i << " is " << (chunk_ptr[i]).newv[0] << std::endl;
    }
    //std::cout << "Slave " << slave_pid << " sending \n";
    MPI_Send(chunk_ptr, bcount, mpi_boid_datatype, MASTER, 0, MPI_COMM_WORLD);
    //std::cout << "Slave " << slave_pid << " sent\n";
  }


}
