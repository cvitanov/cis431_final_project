#include <math.h>
#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include "helper.h"
#include "settings.h"


double camera_location = 1.0;

// Return a random float in the range 0.0 to 1.0.
GLfloat randomFloat() {
  return (GLfloat)rand() / RAND_MAX;
}

// On reshape, uses an orthographic projection with world coordinates ranging
// from 0 to 1 in the x and y directions, and -1 to 1 in z.
void reshape(int width, int height) {
  glViewport(0, 0, width, height);
  float ratio =  ((float) W) / ((float) H); // window aspect ratio
  glMatrixMode(GL_PROJECTION); // projection matrix is active
  if(ORTHO) {
    glOrtho(-xbound,xbound,-ybound,ybound, -ZMAX,ZMAX);
  } else {
    glFrustum( -1.1 * xbound, 1.1 * xbound, -1.1 * ybound, 1.1 * ybound, 0.95*ZMIN,ZMAX);
    double fovy = atan( 2*ybound / ZMIN ) * 180.0 / PI;

  }
  gluLookAt(0.0, 0.0, camera_location, 0.0, 0.0, ZMAX, 0, 1, 0);
  glMatrixMode(GL_MODELVIEW); // return to modelview mode
}

// HELPER FUNCTIONS

// len: returns length of 3D vector
double len(vec a) {
  return sqrt( a.x * a.x + a.y * a.y + a.z * a.z );
}

// dist: returns distance between two vectors
double dist(vec a, vec b) {
  vec temp( a.x - b.x, a.y - b.y, a.z - b.z );
  return len(temp);
}

// dot: returns dot product of two vectors
double dot(vec a, vec b) {
  return (a.x * b.x + a.y * b.y + a.z * b.z);
}


void drawPlane() {

  glPushMatrix();

    glBegin(GL_TRIANGLE_STRIP);

      glVertex3f(-7.0, 0.0, 2.0);
      glVertex3f(-1.0, 0.0, 3.0);
      glVertex3f(-1.0, 7.0, 3.0);
      /* left side */
      glVertex3f(0.0, 0.0, 0.0);
      glVertex3f(0.0, 8.0, 0.0);
      /* right side */
      glVertex3f(1.0, 0.0, 3.0);
      glVertex3f(1.0, 7.0, 3.0);
      /* final tip of right wing */
      glColor3f(0.0, 0.0, 0.0);
      glVertex3f(7.0, 0.0, 2.0);

    glEnd();

  glPopMatrix();
}
